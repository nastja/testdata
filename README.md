# NAStJA test data
This repository contains reference test data for testing for the NAStJA Framework, see https://gitlab.com/nastja/nastja.

Documentation of the testing environment can be found under the NAStJA documentation.

For an up-to-date version of this readme, see the master branch.


